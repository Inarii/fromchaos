
#include "PvPMgr.h"
#include "Timer.h"
#include "Groups/Group.h"
#include "Entities/Object.h"
#include "Log.h"
#include "Chat/Chat.h"
#include "Globals/SharedDefines.h"

PvPMgr::PvPMgr(Player* player)
{
	m_owner = player;
}

PvPMgr::~PvPMgr()
{

}

void PvPMgr::Clear()
{
	m_kills.clear();
}

void PvPMgr::HandlePvPKill(Player* victim, bool killer)
{
	if (!victim || m_owner == victim)
		return;

	if (m_owner->GetBattleGround() && m_owner->GetBattleGround()->isArena())
		return;

	// kill streak
	uint32 Now = WorldTimer::getMSTime();
	m_kills.emplace_back(KillData(victim->GetObjectGuid(), Now));

	if (uint32 gain = CalculateMoneyGain(killer))
	{
		uint32 killCount = m_kills.size();
		std::string moneySuffix = gain >= 10000 ? "Gold" : "Silver";
		uint32 shortGain = gain >= 10000 ? gain / 10000 : gain / 1000;

		m_owner->ModifyMoney(gain);
		ChatHandler(m_owner).PSendSysMessage("+%u %s for killing %s! Killstreak: %u", shortGain, moneySuffix.c_str(), victim->GetName(), killCount);

		if (victim->GetPvPMgr().GetKillstreak() > 1)
		{
			gain = 1 * GOLD; // placeholder
			moneySuffix = gain >= 10000 ? "Gold" : "Silver";
			shortGain = gain >= 10000 ? gain / 10000 : gain / 1000;

			m_owner->ModifyMoney(gain);
			ChatHandler(m_owner).PSendSysMessage("+%u %s for being on a killing spree!", shortGain, moneySuffix.c_str());
		}

		if (Group* group = m_owner->GetGroup())
		{
			for (auto gItr : group->GetMemberSlots())
			{
				if (Player* pl = ObjectAccessor::FindPlayer(gItr.guid))
				{
					if (pl != m_owner && pl->IsAtGroupRewardDistance(m_owner) && pl->isAlive() && pl->GetTeam() != victim->GetTeam())
					{
						if (uint32 grpGain = CalculateMoneyGain(false))
						{
							moneySuffix = grpGain >= 10000 ? "Gold" : "Silver";
							shortGain = grpGain >= 10000 ? grpGain / 10000 : grpGain / 1000;

							pl->ModifyMoney(grpGain);
							ChatHandler(pl).PSendSysMessage("+%u %s for HK", shortGain, moneySuffix.c_str());
						}
					}
				}
			}
		}
	}
}

void PvPMgr::HandlePvPDeath(Player* killer)
{
	if (!killer || m_owner == killer)
		return;

	if (m_owner->GetBattleGround() && m_owner->GetBattleGround()->isArena())
		return;

	if (int32 loss = CalculateMoneyLoss())
	{
		std::string moneySuffix = loss >= 10000 ? "Gold" : "Silver";
		int32 lossShort = loss >= 10000 ? loss / 10000 : loss / 1000;

		m_owner->ModifyMoney(-loss);
		ChatHandler(m_owner).PSendSysMessage("-%i %s for dying to %s", lossShort, moneySuffix.c_str(), killer->GetName());
	}

	// kill streak
	m_kills.clear();
}

uint32 PvPMgr::CalculateMoneyGain(bool killer)
{
	uint32 killCount = m_kills.size();
	uint32 gain = 0;

	if (killer)
	{
		gain = PVP_MONEY_KILL;
		switch (killCount)
		{
			case 5:
				gain += 5 * GOLD;
				break;
			case 10:
				gain += 10 * GOLD;
				break;
			case 25:
				gain += 25 * GOLD;
				break;
			case 50:
				gain += 50 * GOLD;
				break;
			default:
				break;
		}

		if (killCount >= 2)
			gain += 1 * GOLD;
	}
	else
		gain = PVP_MONEY_ASSIST;

	return gain;
}

int32 PvPMgr::CalculateMoneyLoss()
{
	return PVP_MONEY_LOSS;
}