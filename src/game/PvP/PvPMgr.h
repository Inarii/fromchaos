

#ifndef MANGOSSERVER_PVPMGR_H
#define MANGOSSERVER_PVPMGR_H

#include "Entities/Player.h"
#include "Entities/ObjectGuid.h"
#include "Globals/ObjectAccessor.h"

enum KillRewards
{
    PVP_MONEY_KILL = 1 * GOLD,
    PVP_MONEY_ASSIST = 25 * SILVER,
    PVP_MONEY_LOSS = 50 * SILVER
};

struct KillData
{
    KillData(ObjectGuid _victim, uint32 _when) : victim(_victim), when(_when) { }
    KillData() : victim(ObjectGuid()), when(0) { }
    ObjectGuid victim;
    uint32 when;
};

typedef std::list<KillData> Kills;

class PvPMgr
{
    public:
        PvPMgr(Player* player);
        ~PvPMgr();

        void Clear();

        void HandlePvPKill(Player* victim, bool killer = true);
        void HandlePvPDeath(Player* killer);

        const uint32 GetKillstreak() const { return m_kills.size(); }

        uint32 CalculateMoneyGain(bool killer);
        int32 CalculateMoneyLoss();

    private:
        Player* m_owner;
        Kills m_kills;
};

#endif
