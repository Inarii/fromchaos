/*
    From Chaos Beastmaster Script
*/

#include "BeastmasterScript.h"
#include "Chat/Chat.h"
#include "Entities\Pet.h"
#include <Maps\MapManager.h>

bool GossipHello_BeastmasterScript(Player* player, Creature* creature)
{
    player->PlayerTalkClass->ClearMenus();
    player->PrepareQuestMenu(creature->GetObjectGuid());

    if (player->getClass() != CLASS_HUNTER)
    {
        player->CLOSE_GOSSIP_MENU();
        return false;
    }

    auto AddGossipItems = [player](std::string text, uint32 action)
    {
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, text.c_str(), GOSSIP_SENDER_MAIN, action);
    };

    std::list<int> GossipMenu = GetGossipMenu_BeastmasterScript(GOSSIP_SENDER_MAIN);
    for (std::list<int>::iterator itr = GossipMenu.begin(); itr != GossipMenu.end(); itr++)
        AddGossipItems(GetGossipText_BeastmasterScript(GOSSIP_SENDER_MAIN, *itr), *itr);

    player->SEND_GOSSIP_MENU(1, creature->GetObjectGuid());
    return true;
}

// This function is called when the player clicks an option on the gossip menu
// In this case here the faction change could be handled by world-DB gossip, hence it should be handled there!
bool GossipSelect_BeastmasterScript(Player* player, Creature* creature, uint32 sender, uint32 action)
{
    switch (sender)
    {
        case GOSSIP_SENDER_MAIN:
        {
            switch (action)
            {
                case GOSSIP_MENU_MAIN:
                    GossipHello_BeastmasterScript(player, creature);
                    break;
				case GOSSIP_MENU_PETS:
					ShowPets_BeastmasterScript(player, creature, action);
					break;
                case GOSSIP_MENU_STABLES:
					player->GetSession()->SendStablePet(creature->GetObjectGuid());
                    break;
                case GOSSIP_MENU_PETS_SKILLS:
					ResetPetSkills_BeastmasterScript(player);
                    break;
                default:
                    break;
            }
            break;
        }

        case GOSSIP_MENU_PETS:
		{
            switch (action)
            {
                case GOSSIP_MENU_BACK:
                    GossipHello_BeastmasterScript(player, creature);
                    break;
                default:
                    ShowPets_BeastmasterScript(player, creature, action);
                    break;
            }
            break;
		}
		case PET_OPTION_CUNNING:
		case PET_OPTION_FEROCITY:
		case PET_OPTION_TENACITY:
		{
            switch (action)
            {
                case GOSSIP_MENU_BACK:
                    ShowPets_BeastmasterScript(player, creature, GOSSIP_MENU_PETS);
                    break;
                default:
                    CreatePet_BeastmasterScript(player, action);
                    break;
            }
            break;
		}
    }

    return true;
}

bool ShowPets_BeastmasterScript(Player* player, Creature* creature, uint32 petOption)
{
    player->PlayerTalkClass->ClearMenus();

    auto AddGossipItems = [player, petOption](std::string text, uint32 action)
    {
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, text.c_str(), petOption, action);
    };

    std::list<int> GossipMenu = GetGossipMenu_BeastmasterScript(petOption);
    for (std::list<int>::iterator itr = GossipMenu.begin(); itr != GossipMenu.end(); itr++)
        AddGossipItems(GetGossipText_BeastmasterScript(petOption, *itr), *itr);

    player->SEND_GOSSIP_MENU(1, creature->GetObjectGuid());
    return true;
}

bool CreatePet_BeastmasterScript(Player* player, uint32 petId)
{
    player->CLOSE_GOSSIP_MENU();

    Pet* pet = player->CreateTamedPetFrom(petId, 0);
    if (!pet)
    {
        ChatHandler(player).PSendSysMessage("Unable to create pet from Id: %u", petId);
        return false;
    }

    if (Pet* playerPet = player->GetPet())
    {
        player->Uncharm(playerPet);
        player->RemovePet(PET_SAVE_AS_DELETED);
    }

    #define MAX_HAPPINESS 1050000

    pet->SetTP(350);
    pet->SetLoyaltyLevel(BEST_FRIEND);
    pet->SetPower(POWER_HAPPINESS, MAX_HAPPINESS);
    player->PetSpellInitialize();

    pet->SavePetToDB(PET_SAVE_AS_CURRENT, player);
    return true;
}

bool ResetPetSkills_BeastmasterScript(Player* player)
{
    player->CLOSE_GOSSIP_MENU();

    Pet* pet = player->GetPet();
    if (!pet)
    {
        ChatHandler(player).PSendSysMessage("You do not have a pet.");
        return false;
    }

    WorldPacket data(SMSG_PET_UNLEARN_CONFIRM, (8 + 4));
    data << ObjectGuid(pet->GetObjectGuid());
    data << uint32(0);
    player->GetSession()->SendPacket(data);
    return true;
}

void AddSC_BeastmasterScript()
{
    Script* pNewScript = new Script;
    pNewScript->Name = "BeastmasterScript";
    pNewScript->pGossipHello = &GossipHello_BeastmasterScript;
    pNewScript->pGossipSelect = &GossipSelect_BeastmasterScript;
    pNewScript->RegisterSelf(false);
}
