/*
	From Chaos Teleporter Script
*/

#include "TeleporterScript.h"
#include <Maps\MapManager.h>

bool GossipHello_TeleporterScript(Player* player, Creature* creature)
{
    player->PlayerTalkClass->ClearMenus();

    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Help Me", GOSSIP_SENDER_MAIN, GOSSIP_SENDER_MAIN);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Teleport", GOSSIP_SENDER_MAIN, TELEPORT_MAIN);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Queue", GOSSIP_SENDER_MAIN, QUEUE_MAIN);

    player->SEND_GOSSIP_MENU(1, creature->GetObjectGuid());
    return true;
}

// This function is called when the player clicks an option on the gossip menu
// In this case here the faction change could be handled by world-DB gossip, hence it should be handled there!
bool GossipSelect_TeleporterScript(Player* player, Creature* creature, uint32 sender, uint32 action)
{
    switch (sender)
    {
        case GOSSIP_SENDER_MAIN:
        {
            switch (action)
            {
                case HELP_MAIN:
                    break;
                case TELEPORT_MAIN:
                    HandleGossip_TeleporterScript(player, creature, sender, action);
                    break;
                case QUEUE_MAIN:
                    HandleGossip_TeleporterScript(player, creature, sender, action);
                    break;
                default:
                    break;
            }
            break;
        }
        case HELP_MAIN:
        {
            break;
        }
        case TELEPORT_MAIN:
        {
            switch (action)
            {
                case GOSSIP_OPTION_BACK:
                    GossipHello_TeleporterScript(player, creature);
                    break;
                default:
                    Teleport_TeleporterScript(player, action);
                    break;
            }
            break;
        }
        case QUEUE_MAIN:
        {
            switch (action)
            {
                case GOSSIP_OPTION_BACK:
                    GossipHello_TeleporterScript(player, creature);
                    break;
                default:
                    Queue_TeleporterScript(player, creature, action);
                    break;
            }
            break;
        }
    }

    return true;
}

bool HandleGossip_TeleporterScript(Player* player, Creature* creature, uint32 sender, uint32 action)
{
    player->PlayerTalkClass->ClearMenus();

    uint32 faction = player->GetTeam();

    switch (sender)
    {
        case GOSSIP_SENDER_MAIN:
        {
            switch (action)
            {
                case HELP_MAIN:
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Help Me Text", HELP_MAIN, HELP_MAIN);

                    

                    break;
                }
                case TELEPORT_MAIN:
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Karazhan Mall", HELP_MAIN, HELP_MAIN);
                    
                    switch (faction)
                    {
                        case ALLIANCE:
                        {
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Dun Baldar", TELEPORT_MAIN, TELEPORT_AV_STRONGHOLD);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Stonehearth Outpost", TELEPORT_MAIN, TELEPORT_AV_OUTPOST);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Lumbermill", TELEPORT_MAIN, TELEPORT_AV_LUMBERMILL);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Frostwolf Stables", TELEPORT_MAIN, TELEPORT_AV_STABLES);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Thrall", TELEPORT_MAIN, TELEPORT_AV_BOSS);
                            break;
                        }
                        case HORDE:
                        {
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Frostwolf Keep", TELEPORT_MAIN, TELEPORT_AV_STRONGHOLD);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Stonehearth Outpost", TELEPORT_MAIN, TELEPORT_AV_OUTPOST);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Stormpike Lumbermill", TELEPORT_MAIN, TELEPORT_AV_LUMBERMILL);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Stables", TELEPORT_MAIN, TELEPORT_AV_STABLES);
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Vanndar", TELEPORT_MAIN, TELEPORT_AV_BOSS);
                            break;
                        }
                        default:
                            break;
                    }

                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Back", TELEPORT_MAIN, GOSSIP_OPTION_BACK);
                    break;
                }
                case QUEUE_MAIN:
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Arenas", QUEUE_MAIN, QUEUE_ARENAS);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Warsong Gulch", QUEUE_MAIN, QUEUE_WSG);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Arathi Basin", QUEUE_MAIN, QUEUE_AB);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Back", QUEUE_MAIN, GOSSIP_OPTION_BACK);
                    break;
                }
            }
            break;
        }
        case HELP_MAIN:
        {
            switch (action)
            {

            }
            break;
        }
    }

    player->SEND_GOSSIP_MENU(1, creature->GetObjectGuid());
    return true;
}

bool HandleText_TeleporterScript(Player* player, Creature* creature, uint32 sender, uint32 action)
{
    player->PlayerTalkClass->ClearMenus();

    switch (sender)
    {

    }

    player->SEND_GOSSIP_MENU(1, creature->GetObjectGuid());
    return true;
}

void Teleport_TeleporterScript(Player* player, uint32 action)
{
    WorldLocation dest;

    uint32 faction = player->GetTeam();

    switch (action)
    {
        case TELEPORT_KARAZHAN_MALL:
        {
            dest = KarazhanMall;
            break;
        }
        case TELEPORT_AV_STRONGHOLD:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = AlteracValleyDunBaldar;
                    break;
                case HORDE:
                    dest = AlteracValleyFrostwolfKeep;
                    break;
                default:
                    break;
            }
            break;
        }
        case TELEPORT_AV_LUMBERMILL:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = AlteracValleyLumbermillDefensive;
                    break;
                case HORDE:
                    dest = AlteracValleyLumbermillOffensive;
                    break;
                default:
                    break;

            }
            break;
        }
        case TELEPORT_AV_OUTPOST:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = AlteracValleyStonehearthOutpost;
                    break;
                case HORDE:
                    dest = AlteracValleyIcebloodGarrison;
                    break;
                default:
                    break;
            }
            break;
        }
        case TELEPORT_PVP_ZONE:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = PvPZoneAlliance;
                    break;
                case HORDE:
                    dest = PvPZoneHorde;
                    break;
                default:
                    break;
            }
            break;
        }
        case TELEPORT_NAGRAND_FFA:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = NagrandFFAAlliance;
                    break;
                case HORDE:
                    dest = NagrandFFAHorde;
                    break;
                default:
                    break;
            }
            break;
        }
        case TELEPORT_BLADES_FFA:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = BladesEdgeFFAAlliance;
                    break;
                case HORDE:
                    dest = BladesEdgeFFAHorde;
                    break;
                default:
                    break;
            }
            break;
        }
        case TELEPORT_HALAA:
        {
            switch (faction)
            {
                case ALLIANCE:
                    dest = HalaaAlliance;
                    break;
                case HORDE:
                    dest = HalaaHorde;
                    break;
                default:
                    break;
            }
            break;
        }

    }

    if (!MapManager::IsValidMapCoord(dest.mapid, dest.coord_x, dest.coord_y, dest.coord_z, dest.orientation))
        return;

    player->CLOSE_GOSSIP_MENU();
    player->CastSpell(player, 26638, TRIGGERED_OLD_TRIGGERED); // Teleportation Visual Effect
    player->TeleportTo(dest);
}

bool Queue_TeleporterScript(Player* player, Creature* creature, uint32 action)
{
    player->PlayerTalkClass->ClearMenus();

    switch (action)
    {
        case QUEUE_ARENAS:
            player->PlayerTalkClass->SendGossipMenu(10024, creature->GetObjectGuid());
            break;
        case QUEUE_AB:
            player->PlayerTalkClass->SendGossipMenu(7642, creature->GetObjectGuid());
            break;
        case QUEUE_WSG:
            player->PlayerTalkClass->SendGossipMenu(7599, creature->GetObjectGuid());
            break;
        default:
            break;
    }

    player->SEND_GOSSIP_MENU(1, creature->GetObjectGuid());
    return true;
}

void AddSC_TeleporterScript()
{
    Script* pNewScript = new Script;
    pNewScript->Name = "TeleporterScript";
    pNewScript->pGossipHello = &GossipHello_TeleporterScript;
    pNewScript->pGossipSelect = &GossipSelect_TeleporterScript;
    pNewScript->RegisterSelf(false);
}
