/*
    From Chaos Beastmaster Script
*/

#include "AI/ScriptDevAI/include/sc_common.h"

#ifndef BEASTMASTER_SCRIPT_H
#define BEASTMASTER_SCRIPT_H

/* Enum Defines */
enum GossipOptions_Beastmaster
{
    GOSSIP_MENU_MAIN = 1,

    GOSSIP_MENU_PETS,
    GOSSIP_MENU_PETS_SKILLS,
    GOSSIP_MENU_STABLES,
    GOSSIP_MENU_BACK,

    PET_OPTION_CUNNING,
    PET_OPTION_FEROCITY,
    PET_OPTION_TENACITY,

    GOSSIP_OPTION_BACK
};

enum PetId_Beastmaster
{
	PET_CUNNING_BAT = 8602,           // Monstrous Plaguebat
	PET_CUNNING_BIRD_OF_PREY = 7455,  // Winterspring Owl
	PET_CUNNING_SERPENT = 5225,       // Murk Spitter
	PET_CUNNING_SPIDER = 1821,        // Carrion Lurker
	PET_CUNNING_WIND_SERPENT = 4119,  // Elder Cloud Serpent

	PET_FEROCITY_CAT = 7432,          // Frostsaber Stalker
	PET_FEROCITY_CARRION_BIRD = 7376, // Sky Shadow
	PET_FEROCITY_HYENA = 5985,        // Snickerfang Hyena
	PET_FEROCITY_RAPTOR = 6507,       // Ravasaur Hunter
	PET_FEROCITY_TALLSTRIDER = 3068,  // Tallstrider
	PET_FEROCITY_WOLF = 5286,         // Longtooth Runner

	PET_TENACITY_BEAR = 10806,        // Ursius
	PET_TENACITY_BOAR = 4512,         // Rotting Agam'ar
	PET_TENACITY_CRAB = 1088,         // Monstrous Crawler
	PET_TENACITY_CROCOLISK = 3581,    // Sewer Beast
	PET_TENACITY_GORILLA = 6513,      // Un'Goro Stomper
	PET_TENACITY_SCORPID = 5823,      // Death Flayer
	PET_TENACITY_TURTLE = 4397,       // Mudrock Spikeshell
};

/* Method Defines */
bool GossipHello_BeastmasterScript(Player* player, Creature* creature);
bool GossipSelect_BeastmasterScript(Player* player, Creature* creature, uint32 sender, uint32 action);

bool CreatePet_BeastmasterScript(Player* player, uint32 petId);
bool ShowPets_BeastmasterScript(Player* player, Creature* creature, uint32 petOption);
bool ResetPetSkills_BeastmasterScript(Player* player);

std::list<int> GetGossipMenu_BeastmasterScript(uint32 action)
{
    switch (action)
    {
        case GOSSIP_SENDER_MAIN:
            return { GOSSIP_MENU_PETS, GOSSIP_MENU_STABLES, GOSSIP_MENU_PETS_SKILLS };
            break;
        case GOSSIP_MENU_PETS:
            return { PET_OPTION_CUNNING, PET_OPTION_FEROCITY, PET_OPTION_TENACITY, GOSSIP_MENU_BACK };
            break;
        case PET_OPTION_CUNNING:
            return
            {
                PET_CUNNING_BAT,
                PET_CUNNING_BIRD_OF_PREY,
                PET_CUNNING_SERPENT,
                PET_CUNNING_SPIDER,
                PET_CUNNING_WIND_SERPENT,
                GOSSIP_MENU_BACK
            };
            break;
        case PET_OPTION_FEROCITY:
            return
            {
                PET_FEROCITY_CAT,
                PET_FEROCITY_CARRION_BIRD,
                PET_FEROCITY_HYENA,
                PET_FEROCITY_RAPTOR,
                PET_FEROCITY_TALLSTRIDER,
                PET_FEROCITY_WOLF,
                GOSSIP_MENU_BACK
            };
            break;
        case PET_OPTION_TENACITY:
            return
            {
                PET_TENACITY_BEAR,
                PET_TENACITY_BOAR,
                PET_TENACITY_CRAB,
                PET_TENACITY_CROCOLISK,
                PET_TENACITY_GORILLA,
                PET_TENACITY_SCORPID,
                PET_TENACITY_TURTLE,
                GOSSIP_MENU_BACK
            };
            break;
    }
    return { 0 };
}

std::string GetGossipText_BeastmasterScript(uint32 sender, uint32 action)
{
    switch (sender)
    {
        case GOSSIP_SENDER_MAIN:
        {
            switch (action)
            {
                case GOSSIP_MENU_PETS:        return "I'd like to tame a pet"; break;
                case GOSSIP_MENU_STABLES:     return "Pet Stables";            break;
                case GOSSIP_MENU_PETS_SKILLS: return "Reset pet skills";       break;
            }
            break;
        }
        case GOSSIP_MENU_PETS:
        {
            switch (action)
            {
                case PET_OPTION_CUNNING:  return "Tame a Cunning Pet";  break;
                case PET_OPTION_FEROCITY: return "Tame a Ferocity Pet"; break;
                case PET_OPTION_TENACITY: return "Tame a Tenacity Pet"; break;
            }
            break;
        }
        case PET_OPTION_CUNNING:
        {
            switch (action)
            {
                case PET_CUNNING_BAT:          return "Bat";          break;
                case PET_CUNNING_BIRD_OF_PREY: return "Bird of Prey"; break;
                case PET_CUNNING_SERPENT:      return "Serpent";      break;
                case PET_CUNNING_SPIDER:       return "Spider";       break;
                case PET_CUNNING_WIND_SERPENT: return "Wind Serpent"; break;
            }
            break;
        }
        case PET_OPTION_FEROCITY:
        {
            switch (action)
            {
                case PET_FEROCITY_CAT:          return "Cat";          break;
                case PET_FEROCITY_CARRION_BIRD: return "Carrion Bird"; break;
                case PET_FEROCITY_HYENA:        return "Hyena";        break;
                case PET_FEROCITY_RAPTOR:       return "Raptor";       break;
                case PET_FEROCITY_TALLSTRIDER:  return "Tallstrider";  break;
                case PET_FEROCITY_WOLF:         return "Wolf";         break;
            }
            break;
        }
        case PET_OPTION_TENACITY:
        {
            switch (action)
            {
                case PET_TENACITY_BEAR:      return "Bear";      break;
                case PET_TENACITY_BOAR:      return "Boar";      break;
                case PET_TENACITY_CRAB:      return "Crab";      break;
                case PET_TENACITY_CROCOLISK: return "Crocolisk"; break;
                case PET_TENACITY_GORILLA:   return "Gorilla";   break;
                case PET_TENACITY_SCORPID:   return "Scorpid";   break;
                case PET_TENACITY_TURTLE:    return "Turtle";    break;
            }
            break;
        }
    }
    switch (action)
    {
        case GOSSIP_MENU_BACK:
            return "Back";
            break;
    }

    return "";
}

#endif
