/*
    From Chaos Teleporter Script
*/

#include "AI/ScriptDevAI/include/sc_common.h"

#ifndef TELEPORTER_SCRIPT_H
#define TELEPORTER_SCRIPT_H

/* Enum Defines */
enum GossipOptions_Teleporter
{
    GOSSIP_OPTION_MAIN = 1,

    TELEPORT_MAIN,
    TELEPORT_MALL,
    TELEPORT_KARAZHAN_MALL,
    TELEPORT_AV_STRONGHOLD, /* Thrall & Vanndar Stormpike*/
    TELEPORT_AV_OUTPOST, /* Stonehearth Outpost & Iceblood Garrison */
    TELEPORT_AV_LUMBERMILL, /* Defensive & Offensive */
    TELEPORT_AV_STABLES, /* Defensive & Offensive */
    TELEPORT_AV_BOSS,
    TELEPORT_PVP_ZONE,
    TELEPORT_NAGRAND_FFA,
    TELEPORT_BLADES_FFA,
    TELEPORT_HALAA,

    QUEUE_MAIN,
    QUEUE_ARENAS,
    QUEUE_AB,
    QUEUE_WSG,
    QUEUE_EOTS,

    HELP_MAIN,

    GOSSIP_OPTION_BACK
};

/* Method Defines */
bool GossipHello_TeleporterScript(Player* player, Creature* creature);
bool GossipSelect_TeleporterScript(Player* player, Creature* creature, uint32 sender, uint32 action);
bool HandleGossip_TeleporterScript(Player* player, Creature* creature, uint32 sender, uint32 action);
bool HandleText_TeleporterScript(Player* player, Creature* creature, uint32 sender, uint32 action);
void Teleport_TeleporterScript(Player* player, uint32 action);
bool Queue_TeleporterScript(Player* player, Creature* creature, uint32 action);

/* WorldLocations */
    /*
        Usage:
        WorldLocation location(MapId, Xf, Yf, Zf, Of);
        Example:
                               Id X     Y     Z     O
        WorldLocation location(0, 0.0f, 0.0f, 0.0f, 0.0f);
    */

/* Genneral */
WorldLocation KarazhanMall(532, -10969.894531f, -1994.667114f, 80.5f, 1.4540f);

/* Alterac Valley */
/* Alliance */
WorldLocation AlteracValleyDunBaldar(30, 707.0975f, -15.156f, 51.0f, 0.2406f);
WorldLocation AlteracValleyStonehearthOutpost(30, -31.81f, -291.61f, 16, 2.947f);
WorldLocation AlteracValleyLumbermillDefensive(30, 476.681f, -486.072f, 66.0f, 1.6372f);
WorldLocation AlteracValleyStablesOffensive(30, 1.4540f, -614.6356f, 52.0f, 1.5945f);
/* Horde*/
WorldLocation AlteracValleyFrostwolfKeep(30, -1370.8800f, -220.207993f, 99.5f, 5.1312f);
WorldLocation AlteracValleyIcebloodGarrison(30, -537.35f, -168.42f, 58.0f, 2.765f);
WorldLocation AlteracValleyLumbermillOffensive(30, 332.69f, -503.25f, 72.0f, 0.125f);
WorldLocation AlteracValleyStablesDefensive(30, -1261.2719f, -603.34f, 56.0f, 1.5129f);
/* Shared */
WorldLocation AlteracValleyVanndar(30, 762.711f, -490.6979f, 98.0f, 6.18f);
WorldLocation AlteracValleyThrall(30, -1362.5141f, -527.851868f, 53.5f, 3.879f);

/* Misc */
WorldLocation PvPZoneAlliance(30, -150.18f, -324.02f, 16.0f, 2.851f);
WorldLocation PvPZoneHorde(30, -321.58f, -230.45f, 19.0f, 5.8f);

WorldLocation NagrandFFAAlliance(530, -1979.389282f, 6558.571289f, 12.0f, 2.2998f);
WorldLocation NagrandFFAHorde(530, -2101.432373f, 6742.919922f, -2.0f, 5.434f);

WorldLocation BladesEdgeFFAAlliance(530, 2916.747070f, 5991.385254f, -1.2819f, 4.1662f);
WorldLocation BladesEdgeFFAHorde(530, 2764.745117f, 5853.833008f, -0.518f, 1.175f);

WorldLocation HalaaAlliance(530, -1604.122192f, 7764.660156f, -20.0f, 1.531918f);
WorldLocation HalaaHorde(530, -1480.126343f, 8075.162109f, -20.0f, 4.018143f);

#endif
