/* This file is part of the ScriptDev2 Project. See AUTHORS file for Copyright information
* This program is free software licensed under GPL version 2
* Please see the included DOCS/LICENSE.TXT for more information */

#include "AI/ScriptDevAI/include/sc_common.h"

// battlegrounds
extern void AddSC_battleground();

// custom
extern void AddSC_TeleporterScript();
extern void AddSC_BeastmasterScript();

// examples
extern void AddSC_example_creature();
extern void AddSC_example_escort();
extern void AddSC_example_gossip_codebox();
extern void AddSC_example_misc();

// world
extern void AddSC_areatrigger_scripts();
extern void AddSC_generic_creature();
extern void AddSC_go_scripts();
extern void AddSC_guards();
extern void AddSC_item_scripts();
extern void AddSC_npc_professions();
extern void AddSC_npcs_special();
extern void AddSC_quests_scripts();
extern void AddSC_spell_scripts();
extern void AddSC_world_map_scripts();

void AddScripts()
{
    // battlegrounds
    AddSC_battleground();

    // custom
    AddSC_TeleporterScript();
    AddSC_BeastmasterScript();

    // examples
    AddSC_example_creature();
    AddSC_example_escort();
    AddSC_example_gossip_codebox();
    AddSC_example_misc();

    // world
    AddSC_areatrigger_scripts();
    AddSC_generic_creature();
    AddSC_go_scripts();
    AddSC_guards();
    AddSC_item_scripts();
    AddSC_npc_professions();
    AddSC_npcs_special();
    AddSC_quests_scripts();
    AddSC_spell_scripts();
    AddSC_world_map_scripts();
}
