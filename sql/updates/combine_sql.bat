@echo off
setlocal EnableDelayedExpansion
set WorldUpdates=combined_world.sql
set AuthUpdates=combined_auth.sql
set CharactersUpdates=combined_characters.sql

if exist %WorldUpdates% del %WorldUpdates%

for %%a in (mangos\*.sql) do (
echo /* >>%WorldUpdates%
echo * %%a >>%WorldUpdates%
echo */ >>%WorldUpdates%
copy/b %WorldUpdates%+"%%a" %WorldUpdates%
echo. >>%WorldUpdates%
echo. >>%WorldUpdates%)

if exist %AuthUpdates% del %AuthUpdates%

for %%a in (realmd\*.sql) do (
echo /* >>%AuthUpdates%
echo * %%a >>%AuthUpdates%
echo */ >>%AuthUpdates%
copy/b %AuthUpdates%+"%%a" %AuthUpdates%
echo. >>%AuthUpdates%
echo. >>%AuthUpdates%)

if exist %CharactersUpdates% del %CharactersUpdates%

for %%a in (characters\*.sql) do (
echo /* >>%CharactersUpdates%
echo * %%a >>%CharactersUpdates%
echo */ >>%CharactersUpdates%
copy/b %CharactersUpdates%+"%%a" %CharactersUpdates%
echo. >>%CharactersUpdates%
echo. >>%CharactersUpdates%)